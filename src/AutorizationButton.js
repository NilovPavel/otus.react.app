import axios from "axios";
import { Component } from "react";

class AutorizationButton extends Component{
    state = {
        persons: []
      }
    
    onAuthority = async (e) =>{

      console.log("---")

      await axios.get(`/users`)
          .then(res => {
            const persons = res.data;
            console.log(persons)
            this.setState({ persons });
          })
    }

    render () {
        return (
                <button onClick={this.onAuthority}>Авторизоваться</button>
        )
        ;
    }
}

export default AutorizationButton;
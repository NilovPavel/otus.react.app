import './App.css'
import './LoginTextBox'
import React, { Component } from 'react'
import LoginTextBox from './LoginTextBox';
import PasswordTextBox from './PasswordTextBox';
import AutorizationButton from './AutorizationButton';

class App extends Component {
  render() {
    return (
      <div className="App">
            <div>
              <label>Login</label>
              <LoginTextBox />
            </div>
            <div>
              <label>Password</label>
              <PasswordTextBox />
            </div>
            <div>
              <AutorizationButton/>
            </div>
      </div>
    );
  }
  }
  

export default App;
